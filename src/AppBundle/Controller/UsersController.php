<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class UsersController extends Controller
{
    /**
     * @Route("/users/profile/{id}", requirements={"id": "\d+"})
     * @Method({"GET", "PUT"})
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function profileAction(int $id)
    {
        $user = $this->getDoctrine()->getRepository(User::class)->find($id);
        $user_session = $this->getUser();
        return $this->render('@App/Users/profile.html.twig', array(
            'user' => $user,
            'user_session' => $user_session
        ));
    }

    /**
     * @Route("/users/follow")
     * @Method({"GET", "PUT"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function followAction(Request $request)
    {
        $id = $request->get('id');
        $user = $this->getDoctrine()->getRepository(User::class)->find($id);
        $follower = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $user->addFollower($follower);
//        $follower->setFollowings($user);
        $em->flush();
        return new JsonResponse([
            'user' => $user,
        ]);
    }

    /**
     * @Route("/users/unfollow")
     * @Method({"GET", "PUT"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function unfollowAction(Request $request)
    {
        $id = $request->get('id');
        $user = $this->getDoctrine()->getRepository(User::class)->find($id);
        $follower = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $user->removeFollower($follower);
        $em->flush();
        return new JsonResponse([
            'id' => $user->getId(),
        ]);
    }

    /**
     * @Route("/users/followers", requirements={"id": "\d+"})
     * @Method({"GET", "PUT"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function followersAction(Request $request)
    {
        $id = $request->get('id');
        $user = $this->getDoctrine()->getRepository(User::class)->find($id);
        return $this->render('@App/Users/followings.html.twig', array(
            'users' => $user->getFollower()
        ));
    }

    /**
     * @Route("/users/followings", requirements={"id": "\d+"})
     * @Method({"GET", "PUT"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function followingsAction(Request $request)
    {
        $id = $request->get('id');
        $user = $this->getDoctrine()->getRepository(User::class)->find($id);
        return $this->render('@App/Users/followings.html.twig', array(
            'users' => $user->getFollowings()
        ));
    }



}
