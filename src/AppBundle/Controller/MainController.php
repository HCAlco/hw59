<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Comment;
use AppBundle\Entity\Post;
use AppBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Vich\UploaderBundle\Form\Type\VichFileType;

class MainController extends Controller
{
    /**
     * @Route("/index")
     * @Method({"GET", "PUT"})
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        if(!$this->getUser()){
            return $this->redirectToRoute('homepage');
        }
        $user = $this->getUser();
        $images = $this->getDoctrine()->getRepository(Post::class)->findAll();

        return $this->render('@App/Main/index.html.twig', array(
            'images' => $images,
            'user' => $user
        ));
    }
    /**
     * @Route("/images/new")
     * @Method({"GET","HEAD", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function newImageAction(Request $request)
    {
        $post = new Post();

        $form_builder = $this->createFormBuilder($post);
        $form_builder->add('body', TextType::class, array('label' => 'Текст вашей картиночки'));
        $form_builder->add('imageFile', VichFileType::class, array('label' => 'Ваша картиночка'));
        $form_builder->add('save', SubmitType::class, array('label' => 'Добавить'));
        $form = $form_builder->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $post = $form->getData();
            $post->setUser($this->getUser());

            $em = $this->getDoctrine()->getManager();
            $em->persist($post);
            $em->flush();

            return $this->redirectToRoute("app_main_index");
        }
        return $this->render('@App/Main/newImage.html.twig', array(
            'form' => $form->createView(),
            'user' => $this->getUser()
        ));
    }

    /**
     * @Route("/pluslike")
     * @Method({"GET", "PUT", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function likeAction(Request $request)
    {
        $id = $request->get('id');
        $image = $this->getDoctrine()->getRepository(Post::class)->find($id);
        $user = $this->getUser();
        $image->addLiker($user);
        $user->addLikedPosts($image);
        $em = $this->getDoctrine()->getManager();
        $em->flush($image);
        return new JsonResponse([
            'likes' => $image->getCountLikers(),
            'imageId' => $image->getId()
        ]);
    }

    /**
     * @Route("/dislike")
     * @Method({"GET", "PUT", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function dislikeAction(Request $request)
    {
        $id = $request->get('id');
        $image = $this->getDoctrine()->getRepository(Post::class)->find($id);
        $user = $this->getUser();
        $image->removeLiker($user);
        $user->removeLikedPosts($image);
        $em = $this->getDoctrine()->getManager();
        $em->flush($image);
        return new JsonResponse([
            'likes' => $image->getCountLikers(),
            'imageId' => $image->getId()
        ]);
    }

    /**
     * @Route("/comment/new")
     * @Method({"GET", "PUT", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function newCommentAction(Request $request)
    {
        $value = $request->get('value');
        $id = $request->get('id');
        $image = $this->getDoctrine()->getRepository(Post::class)->find($id);
        $user = $this->getUser();
        $comment = new Comment();
        $comment->setComment($value);
        $comment->setUser($user);
        $comment->setPost($image);
        $em = $this->getDoctrine()->getManager();
        $em->persist($comment);
        $em->flush();
        return new JsonResponse([
            'comment' => $comment->getComment(),
            'author' => $user->getUsername(),
            'image' => 'images/avatars/'.$user->getAvatar()
        ]);
    }

    /**
     * @Route("/search")
     * @Method({"GET", "PUT", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function searchAction(Request $request)
    {
        $name = $request->get('name');
        $users = $this->getDoctrine()->getRepository(User::class)->findBy(['username' => $name]);
        return $this->render('@App/Main/search.html.twig', array(
            'users' => $users
        ));
    }

    /**
     * @Route("/posts/liked", requirements={"id": "\d+"})
     * @Method({"GET", "PUT"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function likedPostsAction(Request $request)
    {
        $id = $request->get('id');
        $user = $this->getDoctrine()->getRepository(User::class)->find($id);
        return $this->render('@App/Users/likedPosts.html.twig', array(
            'posts' => $user->getLikedPosts()
        ));
    }

}
