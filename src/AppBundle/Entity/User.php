<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;


/**
 * User
 *
 * @ORM\Table(name="fos_user")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 * @Vich\Uploadable
 */
class User extends BaseUser
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="avatar", type="text", nullable=true, unique=false)
     */
    protected $avatar;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="avatar_file", fileNameProperty="avatar")
     *
     * @var File
     */
    protected $imageFile;


    /**
     * @var Post[]
     *
     * @ORM\OneToMany(targetEntity="Post", mappedBy="user", cascade={"persist", "remove"})
     */
    private $posts;

    /**
     * @var Comment[]
     *
     * @ORM\OneToMany(targetEntity="Comment", mappedBy="user")
     */
    private $comments;


    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="Post", mappedBy="likers")
     */
    private $likedPosts;


    /**
     * @ORM\ManyToMany(targetEntity="User", mappedBy="followers")
     */
    private $followings;

    /**
     * @ORM\ManyToMany(targetEntity="User", inversedBy="followings")
     */
    private $followers;

    public function __construct()
    {
        parent::__construct();
        $this->likedPosts = new ArrayCollection();
    }

    /**
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     *
     * @return User
     */
    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;
        return $this;
    }

    /**
     * @return File|null
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    public function __toString()
    {
        return $this->email ?: '';
    }



    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set avatar
     *
     * @param string $avatar
     *
     * @return User
     */
    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;

        return $this;
    }

    /**
     * Get avatar
     *
     * @return string
     */
    public function getAvatar()
    {
        return $this->avatar;
    }


    /**
     * @param Post[] $post
     * @return User
     */
    public function setPosts(array $post): User
    {
        $this->posts = $post;
        return $this;
    }

    /**
     * @return Post[]
     */
    public function getPosts()
    {
        return $this->posts;
    }

    /**
     * @param Comment[] $comments
     * @return User
     */
    public function setComments(array $comments): User
    {
        $this->comments = $comments;
        return $this;
    }

    /**
     * @return Comment[]
     */
    public function getComments(): array
    {
        return $this->comments;
    }



    /**
     * @return mixed
     */
    public function getFollower()
    {
        return $this->followers;
    }

    /**
     * @param mixed $follower
     * @return User
     */
    public function addFollower($follower)
    {
        $this->followers[] = $follower;
        return $this;
    }

    /**
     * @param mixed $follower
     * @return User
     */
    public function removeFollower($follower)
    {
        $this->followers->removeElement($follower);
        return $this;
    }

    /**
     * @param mixed $followings
     * @return User
     */
    public function setFollowings($followings)
    {
        $this->followings[] = $followings;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFollowings()
    {
        return $this->followings;
    }

    /**
     * @param Post $likedPosts
     * @return User
     */
    public function addLikedPosts($likedPost): User
    {
        $this->likedPosts->add($likedPost);
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getLikedPosts()
    {
        return $this->likedPosts;
    }
    /**
     * @param Post $likedPost
     */
    public function removeLikedPosts($likedPost)
    {
        $this->likedPosts->removeElement($likedPost);
    }
}

